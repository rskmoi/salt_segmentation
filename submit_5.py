import os
import time

import numpy as np
import pandas as pd
from inference_util.inference_result import InferenceResult, SaltResult
import pickle
from PIL import Image

import pydensecrf.densecrf as dcrf
from skimage.io import imread, imsave
from pydensecrf.utils import unary_from_labels, create_pairwise_bilateral
from skimage.color import gray2rgb
from skimage.color import rgb2gray
import matplotlib.pyplot as plt
import glob

def get_infres_list(five_fold_path):
    pickle_paths = sorted(glob.glob(os.path.join(five_fold_path, "*.pickle")))

    infres_list = []
    for pickle_path in pickle_paths:
        print(pickle_path)
        with open(pickle_path, "rb") as f:
            cur_inf_res  = pickle.load(f)
        infres_list.append(cur_inf_res)

    return infres_list

def binary(map, threshold):
    return np.where(map > threshold, 1, 0)


def main():
    use_crf = True
    pickle_path = "../results/5fold_real/"
    infres_list = get_infres_list(pickle_path)

    result_dict_list = []
    threhsold = 0.55
    for i, (cur_res_0, cur_res_1, cur_res_2, cur_res_3, cur_res_4) in enumerate(zip(*infres_list)):
        if i % 100 == 0:
            print("done {} images".format(i))

        cur_name = cur_res_0.name
        # print(cur_name)
        if cur_name != cur_res_1.name or \
            cur_name != cur_res_2.name or \
            cur_name != cur_res_3.name or \
            cur_name != cur_res_4.name:
            raise ValueError

        submit_type = "each"
        if submit_type == "mean":
            cur_map_mean = (cur_res_0.map + cur_res_1.map + cur_res_2.map + cur_res_3.map + cur_res_4.map) * 0.2
            cur_pred = np.where(cur_map_mean > threhsold, 255, 0)
        elif submit_type == "each":
            cur_map_sum = binary(cur_res_0.map, 0.59) + binary(cur_res_1.map, 0.60) + binary(cur_res_2.map, 0.51) + binary(cur_res_3.map, 0.60) + binary(cur_res_4.map, 0.46)
            cur_pred = np.where(cur_map_sum > 2.5, 255, 0)

        Image.fromarray(np.uint8(cur_pred.reshape((101, 101)))).save( "./submit_images/{}_2_before.png".format(cur_name))

        if use_crf:
            cur_image = get_original_image(cur_name)
            cur_pred = crf(cur_image, cur_pred) * 255
            Image.fromarray(np.uint8(cur_image)).save("./submit_images/{}_1_origin.png".format(cur_name))
            Image.fromarray(np.uint8(cur_pred)).save("./submit_images/{}_3_after.png".format(cur_name))

        result_dict_list.append({"id": cur_name,
                                 "rle_mask": RLenc(np.asarray(cur_pred))})

        df = pd.DataFrame(result_dict_list)
        df.to_csv("../results/mirror_mixup0.4_{}_30000.csv".format(threhsold), index=False)

def get_original_image(name):
    image_path = "../data/test/{}.png".format(name)
    return np.asarray(Image.open(image_path))

def crf(original_image, mask_img):
    # Converting annotated image to RGB if it is Gray scale
    mask_img = gray2rgb(mask_img.reshape((101, 101)))

    #Converting the annotations RGB color to single 32 bit integer
    annotated_label = mask_img[:, :, 0] + (mask_img[:, :, 1] << 8) + (mask_img[:, :, 2] << 16)

    # Convert the 32bit integer color to 0,1, 2, ... labels.
    colors, labels = np.unique(annotated_label, return_inverse=True)

    n_labels = 2

    # Setting up the CRF model
    d = dcrf.DenseCRF2D(original_image.shape[1], original_image.shape[0], n_labels)

    # get unary potentials (neg log probability)
    U = unary_from_labels(labels, n_labels, gt_prob=0.7, zero_unsure=False)
    d.setUnaryEnergy(U)

    # This adds the color-independent term, features are the locations only.
    d.addPairwiseGaussian(sxy=(3, 3), compat=3, kernel=dcrf.DIAG_KERNEL,
                          normalization=dcrf.NORMALIZE_SYMMETRIC)

    # Run Inference for 10 steps
    Q = d.inference(10)

    # Find out the most probable class for each pixel.
    MAP = np.argmax(Q, axis=0)

    return MAP.reshape((original_image.shape[0], original_image.shape[1]))


def RLenc(img, order='F', format=True):
    """
    img is binary mask image, shape (r,c)
    order is down-then-right, i.e. Fortran
    format determines if the order needs to be preformatted (according to submission rules) or not

    returns run length as an array or string (if format is True)
    """
    # if np.sum(img) / 255 < 11.0:
    #     return ""

    bytes = img.reshape(img.shape[0] * img.shape[1], order=order)
    runs = []  ## list of run lengths
    r = 0  ## the current run length
    pos = 1  ## count starts from 1 per WK
    for c in bytes:
        if (c == 0):
            if r != 0:
                runs.append((pos, r))
                pos += r
                r = 0
            pos += 1
        else:
            r += 1

    # if last run is unsaved (i.e. data ends with 1)
    if r != 0:
        runs.append((pos, r))
        pos += r
        r = 0

    if format:
        z = ''

        for rr in runs:
            z += '{} {} '.format(rr[0], rr[1])
        return z[:-1]
    else:
        return runs

if __name__ == "__main__":
    main()