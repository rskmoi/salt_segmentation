import os
import time

import numpy as np
import pandas as pd
import tensorflow as tf
from PIL import Image

from dataset.test_dataset import Dataset
# from dataset.dataset_with_depth import Dataset as DepthDataset
from dataset.dataset_with_pred import Dataset as DatasetDepth
from network.u_net import Network


def start_train():
    # dataset = Dataset(img_size=101, is_train=False, batch_size=64)
    dataset = DatasetDepth(img_size=101, is_train=False, batch_size=64, use_set=[5])
    network = Network(is_training=False)


    images, labels, _ = dataset.get_batch()

    images = tf.reshape(images, (-1, 3, 101, 101))

    logits = network.inference(images)

    loss = network.loss(logits, labels)

    _step = 0
    _start_time = time.time()

    summary_writer = tf.summary.FileWriter("./result_out_5_2018-08-06 22:21:31.909788/eval_log")
    saver = tf.train.Saver()
    with tf.train.SingularMonitoredSession() as sess:
        dataset.initialize(sess.raw_session())
        ckpt = tf.train.get_checkpoint_state("./result_out_5_2018-08-06 22:21:31.909788/parameter")
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
        else:
            print('No checkpoint file found')
            return

        loss_list = []
        while not sess.should_stop():
            _loss = sess.run(loss)
            loss_list.append(np.mean(_loss))
            _step += 1
            print(_step)

    mean_loss = np.mean(np.asarray(loss_list))

    print(mean_loss)
    summary_writer.add_summary(tf.Summary(value=[
        tf.Summary.Value(tag="loss", simple_value=mean_loss)]), global_step)

if __name__ == "__main__":
    # img_arr = np.asarray(Image.open("/home/rei/salt/data/train/masks/75efad62c1.png"))
    # print(RLenc(img_arr))
    while True:
        start_train()
        tf.reset_default_graph()
        # tf.get_variable_scope().reuse_variables()
        time.sleep(60 * 10)