import pandas as pd

train_csv_path = "../data/train.csv"
depth_csv_path = "../data/depths_with_total_area.csv"

df_train = pd.read_csv(train_csv_path)

area_dict = {}
for k, v in df_train.iterrows():
    cur_id = v["id"]
    cur_rle_mask = v["rle_mask"]
    if type(cur_rle_mask) is float:
        print("{}, {}".format(cur_id, 0))
        area_dict[cur_id] = 0
        continue

    parsed_rle = cur_rle_mask.split(' ')

    cur_area = 0
    for i, str_digit in enumerate(parsed_rle):
        if i % 2 != 0:
            digit = int(str_digit)
            cur_area += digit

    print("{}, {}".format(cur_id, cur_area))
    area_dict[cur_id] = cur_area

df_depth = pd.read_csv(depth_csv_path)

depth_with_area_list = []
for k, v in df_depth.iterrows():
    cur_id = v["id"]
    cur_depth = v["z"]
    if cur_id in area_dict:
        depth_with_area_list.append({"id": cur_id,
                                     "z": cur_depth,
                                     "area": area_dict[cur_id]})
        print("{}, {}, {}".format(cur_id, cur_depth, area_dict[cur_id]))

depth_with_area = pd.DataFrame(depth_with_area_list)
depth_with_area.to_csv("./test.csv", index=False)