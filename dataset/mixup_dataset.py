import os
import glob
import tensorflow as tf
from PIL import Image
import pandas as pd

class Dataset:
    def __init__(self, img_size, mode, batch_size, use_depths=False, use_set=None):
        if not mode in ["train", "validation", "test"]:
            raise ValueError()

        self.raw_img_size = 101
        self.resize_img_size = 101
        self.img_size = img_size
        self.mode = mode
        self.batch_size = batch_size

        self.use_depths = use_depths
        if use_depths:
            self.depth_dict = self._make_depth_dict()
            print(self.depth_dict)

        self.indice = [1, 2, 3, 4] if use_set is None else use_set

        self._constract()

    def _constract(self):
        self.ts_much_image_paths = tf.placeholder(dtype=tf.string)
        self.ts_much_annotation_paths = tf.placeholder(dtype=tf.string)
        dataset_much = tf.data.Dataset.from_tensor_slices((self.ts_much_image_paths, self.ts_much_annotation_paths))
        self.ts_less_image_paths = tf.placeholder(dtype=tf.string)
        self.ts_less_annotation_paths = tf.placeholder(dtype=tf.string)
        dataset_less = tf.data.Dataset.from_tensor_slices((self.ts_much_image_paths, self.ts_much_annotation_paths))

        dataset_much = dataset_much.shuffle(buffer_size=4000).repeat(None)
        dataset_less = dataset_less.shuffle(buffer_size=4000).repeat(None)
        dataset_much = dataset_much.map(self._decode_image)
        dataset_less = dataset_less.map(self._decode_image)
        dataset_much = dataset_much.map(self._augment)
        dataset_less = dataset_less.map(self._augment)

        dataset_much = dataset_much.batch(batch_size=self.batch_size)
        dataset_less = dataset_less.batch(batch_size=self.batch_size)

        self.iterator_much = dataset_much.make_initializable_iterator()
        self.iterator_less = dataset_less.make_initializable_iterator()

    def _decode(self, ts_image_path, is_image=True):
        ts_image = tf.image.decode_png(tf.read_file(ts_image_path), channels=1)
        ts_image = tf.cast(ts_image, dtype=tf.float32)
        ts_image = tf.transpose(ts_image, (2, 0, 1))
        if is_image:
            ts_image = (ts_image - 128.0) / 128.0
        else:
            ts_image = ts_image / 255.0

        return ts_image

    def _decode_image(self, ts_image_path, ts_annotation_path):
        ts_image = self._decode(ts_image_path)
        ts_ann = self._decode(ts_annotation_path, is_image=False)

        return ts_image, ts_ann

    def _augment(self, ts_image, ts_ann):
        # ここにaugmentを集結
        ts_image, ts_ann = self._random_flip_lr(ts_image, ts_ann)
        ts_image, ts_ann = self._random_contrast(ts_image, ts_ann)

        return ts_image, ts_ann

    def _random_flip_lr(self, ts_image_sliced, ts_ann_sliced):
        rand = tf.random_uniform(shape=())
        ts_image_sliced = tf.cond(rand < 0.5,
                                  lambda: ts_image_sliced,
                                  lambda: tf.reverse(ts_image_sliced, [2]))
        ts_ann_sliced = tf.cond(rand < 0.5,
                                lambda: ts_ann_sliced,
                                lambda: tf.reverse(ts_ann_sliced, [2]))

        return ts_image_sliced, ts_ann_sliced

    def _random_flip_ud(self, ts_image_sliced, ts_ann_sliced):
        rand = tf.random_uniform(shape=())
        ts_image_sliced = tf.cond(rand < 0.5,
                                  lambda: ts_image_sliced,
                                  lambda: tf.reverse(ts_image_sliced, [1]))
        ts_ann_sliced = tf.cond(rand < 0.5,
                                lambda: ts_ann_sliced,
                                lambda: tf.reverse(ts_ann_sliced, [1]))

        return ts_image_sliced, ts_ann_sliced

    def _random_contrast(self, ts_image_sliced, ts_ann_sliced):
        if self.use_depths:
            ts_image = tf.reshape(ts_image_sliced[0], (1, self.img_size, self.img_size))
            ts_depth_image = tf.reshape(ts_image_sliced[1], (1, self.img_size, self.img_size))
            ts_image_contrast = tf.image.random_contrast(ts_image, lower=0.6, upper=1.4)
            ts_image_sliced = tf.concat((ts_image_contrast, ts_depth_image), axis=0)
        else:
            ts_image_sliced = tf.image.random_contrast(ts_image_sliced, lower=0.6, upper=1.4)

        return ts_image_sliced, ts_ann_sliced

    def initialize(self, sess):
        if self.mode in ["train", "validation"]:
            val_much_annotation_paths = []
            for i in self.indice:
                val_much_annotation_paths += (glob.glob("/home/rei/salt/data/mixup_train/masks_{}/much/*.png".format(i)))
            val_much_image_paths = ["/home/rei/salt/data/train_5fold/images/{}".format(os.path.basename(cur_path))
                                    for cur_path in val_much_annotation_paths]

            val_less_annotation_paths = []
            for i in self.indice:
                val_less_annotation_paths += (glob.glob("/home/rei/salt/data/mixup_train/masks_{}/less/*.png".format(i)))
            val_less_image_paths = ["/home/rei/salt/data/train_5fold/images/{}".format(os.path.basename(cur_path))
                                    for cur_path in val_less_annotation_paths]

            sess.run([self.iterator_much.initializer, self.iterator_less.initializer],
                     feed_dict={self.ts_much_image_paths: val_much_image_paths,
                                self.ts_much_annotation_paths: val_much_annotation_paths,
                                self.ts_less_image_paths: val_less_image_paths,
                                self.ts_less_annotation_paths: val_less_annotation_paths})

        print("Find {} images.".format(len(val_much_image_paths)))


    def get_batch(self):
        ts_images_much, ts_anns_much = self.iterator_much.get_next()
        ts_images_less, ts_anns_less = self.iterator_less.get_next()

        ts_shape = tf.shape(ts_images_much)
        random_beta = tf.distributions.Beta(0.2, 0.2).sample(sample_shape=((1, ts_shape[0])))
        random_beta_map_pos_im = tf.tensordot(random_beta, tf.ones_like(ts_images_much), axes=0)[0, :, 0, :, :, :]
        random_beta_map_neg_im = tf.tensordot(1 - random_beta, tf.ones_like(ts_images_less), axes=0)[0, :, 0, :, :, :]

        random_beta_map_pos_an = tf.tensordot(random_beta, tf.ones_like(ts_anns_much), axes=0)[0, :, 0, :, :, :]
        random_beta_map_neg_an = tf.tensordot(1 - random_beta, tf.ones_like(ts_anns_less), axes=0)[0, :, 0, :, :, :]

        ts_images = tf.multiply(ts_images_much, random_beta_map_pos_im) + tf.multiply(ts_images_less, random_beta_map_neg_im)
        ts_anns = tf.multiply(ts_anns_much, random_beta_map_pos_an) + tf.multiply(ts_anns_less, random_beta_map_neg_an)

        return ts_images, ts_anns

    def _make_depth_dict(self):
        depth_dict = {}
        df = pd.read_csv("/home/rei/salt/data/depths.csv")
        for k, v in df.iterrows():
            cur_name = v["id"]
            cur_depth = v["z"]
            depth_dict[cur_name] = cur_depth

        return depth_dict

# dataset = Dataset(img_size=101, mode="train", batch_size=64, use_set=[1,2,3,4], use_depths=True)
# _, __, ___ = dataset.get_batch()
#
# sess = tf.Session()
# dataset.initialize(sess)
#
# sess.run(_)

