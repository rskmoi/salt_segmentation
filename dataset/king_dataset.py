import os
import glob
import tensorflow as tf
from PIL import Image
import pandas as pd

class Dataset:
    def __init__(self, img_size, mode, batch_size, use_depths=False, use_set=None, mirror_pad=False):
        if not mode in ["train", "validation", "test"]:
            raise ValueError()

        self.raw_img_size = 101
        self.resize_img_size = 101
        self.img_size = img_size
        self.mode = mode
        self.mirror_pad = mirror_pad
        self.batch_size = batch_size

        self.use_depths = use_depths
        if use_depths:
            self.depth_dict = self._make_depth_dict()
            print(self.depth_dict)

        self.indice = [1, 2, 3, 4] if use_set is None else use_set

        self._constract()

    def _constract(self):
        self.ts_image_paths = tf.placeholder(dtype=tf.string)
        self.ts_annotation_paths = tf.placeholder(dtype=tf.string)
        self.ts_depths = tf.placeholder(dtype=tf.float32)

        dataset = tf.data.Dataset.from_tensor_slices((self.ts_image_paths, self.ts_annotation_paths, self.ts_depths))

        if self.mode == "train":
            dataset = dataset.shuffle(buffer_size=4000).repeat(None)
            dataset = dataset.map(self._decode_image)
            if self.mirror_pad:
                dataset = dataset.map(self._pad_and_crop)
            else:
                dataset = dataset.map(self._crop)
            dataset = dataset.map(self._augment)
        elif self.mode == "validation":
            dataset = dataset.repeat(1)
            dataset = dataset.map(self._decode_image)
        elif self.mode == "test":
            dataset = dataset.repeat(1)
            dataset = dataset.map(self._decode_image)

        dataset = dataset.batch(batch_size=self.batch_size)
        self.iterator = dataset.make_initializable_iterator()

    def _decode_image(self, ts_image_path, ts_annotation_path, ts_depth):
        ts_image = tf.image.decode_png(tf.read_file(ts_image_path), channels=1)
        ts_image = tf.cast(ts_image, dtype=tf.float32)
        ts_image = tf.transpose(ts_image, (2, 0, 1))
        ts_image = (ts_image - 127.0) / 127.0

        if self.use_depths:
            ts_depth_reguralized = (ts_depth - 506.7) / 208.0
            ts_depths_map = tf.multiply(tf.ones(shape=tf.shape(ts_image)), ts_depth_reguralized)
            ts_image = tf.concat((ts_image, ts_depths_map), axis=0)

        if self.mode == "test":
            return ts_image, ts_image_path

        ts_ann = tf.image.decode_png(tf.read_file(ts_annotation_path), channels=1)
        ts_ann = tf.cast(ts_ann, dtype=tf.float32)
        ts_ann = tf.transpose(ts_ann, (2, 0, 1))
        ts_ann = ts_ann / 255.0

        return ts_image, ts_ann, ts_image_path

    def _augment(self, ts_image, ts_ann, ts_path):
        # ここにaugmentを集結
        ts_image, ts_ann = self._random_flip_lr(ts_image, ts_ann)
        ts_image, ts_ann = self._random_contrast(ts_image, ts_ann)

        return ts_image, ts_ann, ts_path

    def _crop(self, ts_image, ts_ann, ts_image_path):
        if self.resize_img_size == self.img_size:
            return ts_image, ts_ann, ts_image_path

        rand_x = tf.random_uniform(minval=0, maxval=self.resize_img_size - self.img_size, dtype=tf.int32, shape=())
        rand_y = tf.random_uniform(minval=0, maxval=self.resize_img_size - self.img_size, dtype=tf.int32, shape=())

        if self.use_depths:
            ts_image_sliced = tf.slice(ts_image, [0, rand_x, rand_y], [2, self.img_size, self.img_size])
        else:
            ts_image_sliced = tf.slice(ts_image, [0, rand_x, rand_y], [1, self.img_size, self.img_size])

        ts_ann_sliced = tf.slice(ts_ann, [0, rand_x, rand_y], [1, self.img_size, self.img_size])

        return ts_image_sliced, ts_ann_sliced, ts_image_path

    def _pad_and_crop(self, ts_image, ts_ann, ts_image_path):
        pad = 10
        ts_image_pad = tf.pad(ts_image, [[0,0], [pad,pad], [pad,pad]], mode="SYMMETRIC")
        ts_anns_pad =  tf.pad(ts_ann, [[0,0], [pad,pad], [pad,pad]], mode="SYMMETRIC")

        rand_x = tf.random_uniform(minval=0, maxval=pad, dtype=tf.int32, shape=())
        rand_y = tf.random_uniform(minval=0, maxval=pad, dtype=tf.int32, shape=())

        if self.use_depths:
            ts_image_sliced = tf.slice(ts_image_pad, [0, rand_x, rand_y], [2, self.img_size, self.img_size])
        else:
            ts_image_sliced = tf.slice(ts_image_pad, [0, rand_x, rand_y], [1, self.img_size, self.img_size])

        ts_ann_sliced = tf.slice(ts_anns_pad, [0, rand_x, rand_y], [1, self.img_size, self.img_size])

        return ts_image_sliced, ts_ann_sliced, ts_image_path

    def _random_flip_lr(self, ts_image_sliced, ts_ann_sliced):
        rand = tf.random_uniform(shape=())
        ts_image_sliced = tf.cond(rand < 0.5,
                                  lambda: ts_image_sliced,
                                  lambda: tf.reverse(ts_image_sliced, [2]))
        ts_ann_sliced = tf.cond(rand < 0.5,
                                lambda: ts_ann_sliced,
                                lambda: tf.reverse(ts_ann_sliced, [2]))

        return ts_image_sliced, ts_ann_sliced

    def _random_flip_ud(self, ts_image_sliced, ts_ann_sliced):
        rand = tf.random_uniform(shape=())
        ts_image_sliced = tf.cond(rand < 0.5,
                                  lambda: ts_image_sliced,
                                  lambda: tf.reverse(ts_image_sliced, [1]))
        ts_ann_sliced = tf.cond(rand < 0.5,
                                lambda: ts_ann_sliced,
                                lambda: tf.reverse(ts_ann_sliced, [1]))

        return ts_image_sliced, ts_ann_sliced

    def _random_contrast(self, ts_image_sliced, ts_ann_sliced):
        if self.use_depths:
            ts_image = tf.reshape(ts_image_sliced[0], (1, self.img_size, self.img_size))
            ts_depth_image = tf.reshape(ts_image_sliced[1], (1, self.img_size, self.img_size))
            ts_image_contrast = tf.image.random_contrast(ts_image, lower=0.6, upper=1.4)
            ts_image_sliced = tf.concat((ts_image_contrast, ts_depth_image), axis=0)
        else:
            ts_image_sliced = tf.image.random_contrast(ts_image_sliced, lower=0.6, upper=1.4)

        return ts_image_sliced, ts_ann_sliced

    def initialize(self, sess):
        if self.mode in ["train", "validation"]:
            val_annotation_paths = []
            for i in self.indice:
                val_annotation_paths += (glob.glob("/home/rei/salt/data/train_5fold/masks_{}/*.png".format(i)))
            val_image_paths = ["/home/rei/salt/data/train_5fold/images/{}".format(os.path.basename(cur_path))
                               for cur_path in val_annotation_paths]

            if self.use_depths:
                val_depths = [self.depth_dict[os.path.basename(cur_path).replace(".png", "")] for cur_path in
                              val_annotation_paths]
            else:
                val_depths = ["0"] * len(val_annotation_paths)

            sess.run(self.iterator.initializer, feed_dict={self.ts_image_paths: val_image_paths,
                                                           self.ts_annotation_paths: val_annotation_paths,
                                                           self.ts_depths: val_depths})
        else:
            val_image_path = glob.glob("/home/rei/salt/data/test/*.png")
            if self.use_depths:
                val_depths = [self.depth_dict[os.path.basename(cur_path).replace(".png", "")]
                              for cur_path in val_image_path]
            else:
                val_depths = ["0"] * len(val_image_path)

            sess.run(self.iterator.initializer, feed_dict={self.ts_image_paths: val_image_path,
                                                           self.ts_annotation_paths: val_image_path,
                                                           self.ts_depths: val_depths})

        print("Find {} images.".format(len(val_depths)))


    def get_batch(self):
        if self.mode == "train":
            ts_images_sliced_0, ts_anns_sliced_0, ts_path_0 = self.iterator.get_next()

            ts_images_sliced_1, ts_anns_sliced_1, ts_path_1 = self.iterator.get_next()

            ts_shape = tf.shape(ts_images_sliced_0)
            random_beta = tf.distributions.Beta(0.4, 0.4).sample(sample_shape=((ts_shape[0], 1, 1, 1)))
            random_beta_map_pos_im = tf.ones_like(ts_images_sliced_0) * random_beta
            random_beta_map_neg_im = tf.ones_like(ts_images_sliced_0) * (1 - random_beta)
            random_beta_map_pos_an = tf.ones_like(ts_anns_sliced_0) * random_beta
            random_beta_map_neg_an = tf.ones_like(ts_anns_sliced_0) * (1 - random_beta)

            ts_images_sliced = tf.multiply(ts_images_sliced_0, random_beta_map_pos_im) + tf.multiply(ts_images_sliced_1, random_beta_map_neg_im)
            ts_anns_sliced = tf.multiply(ts_anns_sliced_0, random_beta_map_pos_an) + tf.multiply(ts_anns_sliced_1, random_beta_map_neg_an)

            return ts_images_sliced, ts_anns_sliced, tf.string_join([ts_path_0, ts_path_1])
        if self.mode == "validation":
            ts_images_sliced, ts_anns, ts_path = self.iterator.get_next()

            return ts_images_sliced, ts_path
        else:
            ts_images_sliced, ts_path = self.iterator.get_next()

            return ts_images_sliced, ts_path

    def _make_depth_dict(self):
        depth_dict = {}
        df = pd.read_csv("/home/rei/salt/data/depths.csv")
        for k, v in df.iterrows():
            cur_name = v["id"]
            cur_depth = v["z"]
            depth_dict[cur_name] = cur_depth

        return depth_dict

# dataset = Dataset(img_size=101, mode="train", batch_size=64, use_set=[1,2,3,4], use_depths=False, mirror_pad=True)
# im, la, pa = dataset.get_batch()
#
# sess = tf.Session()
# dataset.initialize(sess)
#
# _im, _la, _pa = sess.run([im, la, pa])
#
# print(_im.shape)
# print(_la.shape)
# print(_pa.shape)
# import numpy as np
# # np.set_printoptions(threshold=100000)
# for i in range(64):
#     Image.fromarray(np.uint8(_im[i].reshape(101, 101) * 127.0 + 127.0)).save("../leak_test/{}_hoge.png".format(i))
#     Image.fromarray(np.uint8(_la[i].reshape(101, 101) * 255)).save("../leak_test/{}_hogf.png".format(i))
#     print(_im[i].reshape(101, 101) * 127.0 + 127.0)
#     print(i, _pa[i])

