import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

class Network:
    def __init__(self, is_training):
        self.data_format = "NCHW"
        self.is_training = is_training

    def _variable_on_cpu(self, name, shape, initializer):
        with tf.device('/cpu:0'):
            var = tf.get_variable(name, shape, initializer=initializer, dtype=tf.float32)
        return var

    def _variable_with_weight_decay(self, name, shape, stddev, wd):
        var = self._variable_on_cpu(
            name,
            shape,
            tf.truncated_normal_initializer(stddev=stddev, dtype=tf.float32))

        if wd is not None:
            weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
            tf.add_to_collection('losses', weight_decay)

        return var

    def _conv(self, featmap, ksize, channel_out, name, is_last_layer=False):
        channel_in = featmap.shape[1]
        with tf.variable_scope(name) as scope:
            kernel = self._variable_with_weight_decay('weights',
                                                      shape=[ksize, ksize, channel_in, channel_out],
                                                      stddev=5e-2,
                                                      wd=None)

            featmap = tf.nn.conv2d(featmap, kernel, [1, 1, 1, 1], padding='SAME', data_format=self.data_format)

            if not is_last_layer:
                featmap = tf.contrib.layers.batch_norm(featmap, is_training=self.is_training,
                                                       data_format=self.data_format)
                featmap = tf.nn.relu(featmap, name=scope.name)
            else:
                biases = self._variable_on_cpu("biases", [1], initializer=tf.constant_initializer(0.0))
                featmap = tf.nn.bias_add(featmap, biases, data_format="NCHW")

            return featmap

    def _deconv(self, featmap, ksize, channel_out, output_shape, name):
        channel_in = featmap.shape[1]

        with tf.variable_scope(name) as scope:
            kernel = self._variable_with_weight_decay('weights',
                                                      shape=[ksize, ksize, channel_out, channel_in],
                                                      stddev=5e-2,
                                                      wd=None)

            featmap = tf.nn.conv2d_transpose(featmap, kernel, output_shape, [1, 1, 2, 2], padding='SAME', data_format=self.data_format)

            featmap = tf.contrib.layers.batch_norm(featmap, is_training=self.is_training, data_format=self.data_format)

            featmap = tf.nn.relu(featmap, name=scope.name)

            return featmap

    def _atrous_conv(self, featmap, ksize, channel_out, rate, name):
        channel_in = featmap.shape[3]
        with tf.variable_scope(name) as scope:
            kernel = self._variable_with_weight_decay('weights',
                                                      shape=[ksize, ksize, channel_in, channel_out],
                                                      stddev=5e-2,
                                                      wd=None)

            featmap = tf.nn.atrous_conv2d(featmap, kernel, rate=rate, padding='SAME')

            featmap = tf.contrib.layers.batch_norm(featmap, is_training=self.is_training,
                                                       data_format="NCHW")
            featmap = tf.nn.relu(featmap, name=scope.name)

            return featmap


    def _bottom(self, featmap, ksize, channel_out, name, layers=4):
        featmap = tf.transpose(featmap, (0, 2, 3, 1))
        _atrous_conv_list = []
        for i in range(layers):
            featmap = self._atrous_conv(featmap, ksize, channel_out, rate=2**i, name="{}_{}".format(name, i))
            _atrous_conv_list.append(featmap)

        _sum_atrous = tf.reduce_sum(_atrous_conv_list, axis=0)
        return tf.transpose(_sum_atrous, (0, 3, 1, 2))

    def _pooling(self, featmap, ksize):
        featmap = tf.nn.max_pool(featmap, ksize=[1, 1, ksize, ksize], strides=[1, 1, 2, 2], padding="SAME", data_format=self.data_format)
        return featmap

    def _concat(self, past_layer, new_layer):
        return tf.concat((past_layer, new_layer), axis=1)

    def inference(self, images):
        # conv layer 0
        featmap = self._conv(images, ksize=3, channel_out=64, name="conv1")
        featmap = self._conv(featmap, ksize=3, channel_out=64, name="conv2")
        layer_0 = featmap
        print("shape of layer 0 = {}".format(layer_0.shape))
        featmap = self._pooling(featmap, ksize=2)

        # conv layer 1
        featmap = self._conv(featmap, ksize=3, channel_out=128, name="conv3")
        featmap = self._conv(featmap, ksize=3, channel_out=128, name="conv4")
        layer_1 = featmap
        print("shape of layer 1 = {}".format(layer_1.shape))
        featmap = self._pooling(featmap, ksize=2)

        # conv layer 2
        featmap = self._conv(featmap, ksize=3, channel_out=256, name="conv5")
        featmap = self._conv(featmap, ksize=3, channel_out=256, name="conv6")
        layer_2 = featmap
        print("shape of layer 2 = {}".format(layer_2.shape))
        featmap = self._pooling(featmap, ksize=2)

        # conv layer 3
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv7")
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv8")
        # featmap = self._bottom(featmap, ksize=3, channel_out=512, name="bottom")
        layer_3 = featmap
        print("shape of layer 3 = {}".format(layer_3.shape))
        featmap = self._pooling(featmap, ksize=2)

        # deconv layer 0
        featmap = self._conv(featmap, ksize=3, channel_out=1024, name="conv9")
        featmap = self._conv(featmap, ksize=3, channel_out=1024, name="conv10")
        featmap = self._deconv(featmap, ksize=2, channel_out=512, output_shape=tf.shape(layer_3), name="deconv1")
        print("shape of deconv1 = {}".format(featmap.shape))
        featmap = self._concat(layer_3, featmap)

        # deconv layer 1
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv11")
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv12")
        featmap = self._deconv(featmap, ksize=2, channel_out=256, output_shape=tf.shape(layer_2), name="deconv2")
        print("shape of deconv2 = {}".format(featmap.shape))
        featmap = self._concat(layer_2, featmap)

        # deconv layer 2
        featmap = self._conv(featmap, ksize=3, channel_out=256, name="conv13")
        featmap = self._conv(featmap, ksize=3, channel_out=256, name="conv14")
        featmap = self._deconv(featmap, ksize=2, channel_out=128, output_shape=tf.shape(layer_1), name="deconv3")
        print("shape of deconv3 = {}".format(featmap.shape))
        featmap = self._concat(layer_1, featmap)

        # deconv layer 3
        featmap = self._conv(featmap, ksize=3, channel_out=128, name="conv15")
        featmap = self._conv(featmap, ksize=3, channel_out=128, name="conv16")
        featmap = self._deconv(featmap, ksize=2, channel_out=64, output_shape=tf.shape(layer_0), name="deconv4")
        print("shape of deconv4 = {}".format(featmap.shape))
        featmap = self._concat(layer_0, featmap)

        # last layer
        featmap = self._conv(featmap, ksize=3, channel_out=64, name="conv17")
        featmap = self._conv(featmap, ksize=3, channel_out=64, name="conv18")
        featmap = self._conv(featmap, ksize=1, channel_out=1, name="conv19", is_last_layer=True)
        print("shape of output = {}".format(featmap.shape))

        return featmap

    def loss(self, logits, labels):
        cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=logits)
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
        tf.add_to_collection('losses', cross_entropy_mean)

        return tf.add_n(tf.get_collection('losses'), name='total_loss')

    def dice_loss(self, logits, labels):
        sigmoid_logits = tf.sigmoid(logits)
        sum_sigmoid_logits = tf.reduce_sum(sigmoid_logits, axis=0)
        sum_labels = tf.reduce_sum(labels, axis=0)
        intersect = tf.reduce_sum(tf.multiply(sigmoid_logits, labels), axis=0)
        dice_losses = 1.0 - ((2 * intersect + 1.0) / (sum_sigmoid_logits + sum_labels + 1.0))
        dice_loss_mean = tf.reduce_mean(dice_losses)
        tf.add_to_collection('losses', dice_loss_mean)

        return tf.add_n(tf.get_collection('losses'), name='total_loss')


# network = Network(is_training=True)
# network.inference(tf.zeros([64, 1, 90, 90]))