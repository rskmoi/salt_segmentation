import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

class Network:
    def __init__(self, is_training):
        self.data_format = "NCHW"
        self.is_training = is_training

    def _variable_on_cpu(self, name, shape, initializer):
        with tf.device('/cpu:0'):
            var = tf.get_variable(name, shape, initializer=initializer, dtype=tf.float32)
        return var

    def _variable_with_weight_decay(self, name, shape, stddev, wd):
        var = self._variable_on_cpu(
            name,
            shape,
            tf.truncated_normal_initializer(stddev=stddev, dtype=tf.float32))

        if wd is not None:
            weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
            tf.add_to_collection('losses', weight_decay)

        return var

    def _conv(self, featmap, ksize, channel_out, name, is_last_layer=False):
        channel_in = featmap.shape[1]
        with tf.variable_scope(name) as scope:
            kernel = self._variable_with_weight_decay('weights',
                                                      shape=[ksize, ksize, channel_in, channel_out],
                                                      stddev=5e-2,
                                                      wd=None)

            featmap = tf.nn.conv2d(featmap, kernel, [1, 1, 1, 1], padding='SAME', data_format=self.data_format)

            featmap = tf.contrib.layers.batch_norm(featmap, is_training=self.is_training, data_format=self.data_format)

            if not is_last_layer:
                featmap = tf.nn.relu(featmap, name=scope.name)

            return featmap

    def _pooling(self, featmap, ksize):
        featmap = tf.nn.max_pool(featmap, ksize=[1, 1, ksize, ksize], strides=[1, 1, 2, 2], padding="SAME", data_format=self.data_format)
        return featmap

    def _fc(self, featmap, channel_out, name):
        with tf.variable_scope(name) as scope:
            channel_in = featmap.shape[1]
            kernel = self._variable_with_weight_decay('weights',
                                                      shape=[channel_in, channel_out],
                                                      stddev=5e-2,
                                                      wd=None)

            biases = self._variable_on_cpu("biases", shape=[channel_out], initializer=tf.constant_initializer())

            featmap = tf.matmul(featmap, kernel) + biases

            return featmap


    def inference(self, images):
        # conv layer 0
        featmap = self._conv(images, ksize=3, channel_out=64, name="conv1")
        featmap = self._conv(featmap, ksize=3, channel_out=64, name="conv2")
        featmap = self._pooling(featmap, ksize=2)

        # conv layer 1
        featmap = self._conv(featmap, ksize=3, channel_out=128, name="conv3")
        featmap = self._conv(featmap, ksize=3, channel_out=128, name="conv4")
        featmap = self._pooling(featmap, ksize=2)

        # conv layer 2
        featmap = self._conv(featmap, ksize=3, channel_out=256, name="conv5")
        featmap = self._conv(featmap, ksize=3, channel_out=256, name="conv6")
        featmap = self._conv(featmap, ksize=3, channel_out=256, name="conv7")
        featmap = self._pooling(featmap, ksize=2)

        # conv layer 3
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv8")
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv9")
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv10")
        featmap = self._pooling(featmap, ksize=2)

        # conv layer 3
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv11")
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv12")
        featmap = self._conv(featmap, ksize=3, channel_out=512, name="conv13")
        featmap = self._pooling(featmap, ksize=2)

        # fc1
        featmap = tf.reshape(featmap, [-1, 512*4*4])
        featmap = self._fc(featmap, channel_out=4096, name="fc1")
        featmap = tf.nn.relu(featmap)
        # fc2
        featmap = self._fc(featmap, channel_out=4096, name="fc2")
        featmap = tf.nn.relu(featmap)
        # fc3
        featmap = self._fc(featmap, channel_out=2, name="fc3")

        print(featmap.shape)
        return featmap

    def loss(self, logits, labels):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=logits)
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
        tf.add_to_collection('losses', cross_entropy_mean)

        return tf.add_n(tf.get_collection('losses'), name='total_loss')

# network = Network(is_training=True)
# network.inference(tf.zeros([64, 1, 101, 101]))