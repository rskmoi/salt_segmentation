import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

class Network:
    def __init__(self, is_training):
        self.data_format = "NHWC"
        self.is_training = is_training

    def _variable_on_cpu(self, name, shape, initializer):
        with tf.device('/cpu:0'):
            var = tf.get_variable(name, shape, initializer=initializer, dtype=tf.float32)
        return var

    def _variable_with_weight_decay(self, name, shape, stddev, wd):
        var = self._variable_on_cpu(
            name,
            shape,
            tf.truncated_normal_initializer(stddev=stddev, dtype=tf.float32))

        if wd is not None:
            weight_decay = tf.multiply(tf.nn.l2_loss(var), wd, name='weight_loss')
            tf.add_to_collection('losses', weight_decay)

        return var

    def _conv(self, featmap, ksize, channel_out, name, stride=1, relu=True):
        channel_in = featmap.shape[3]
        with tf.variable_scope(name) as scope:
            kernel = self._variable_with_weight_decay('weights',
                                                      shape=[ksize, ksize, channel_in, channel_out],
                                                      stddev=5e-2,
                                                      wd=None)

            featmap = tf.nn.conv2d(featmap, kernel, [1, stride, stride, 1], padding='VALID', data_format=self.data_format)


            featmap = tf.contrib.layers.batch_norm(featmap, is_training=self.is_training,
                                                   data_format=self.data_format)

            if relu:
                featmap = tf.nn.relu(featmap, name=scope.name)

            return featmap

    def _atrous_conv(self, featmap, ksize, channel_out, rate, name, relu=True):
        channel_in = featmap.shape[3]
        with tf.variable_scope(name) as scope:
            kernel = self._variable_with_weight_decay('weights',
                                                      shape=[ksize, ksize, channel_in, channel_out],
                                                      stddev=5e-2,
                                                      wd=None)

            featmap = tf.nn.atrous_conv2d(featmap, kernel, rate=rate, padding='VALID')

            featmap = tf.contrib.layers.batch_norm(featmap, is_training=self.is_training,
                                                       data_format="NCHW")

            if relu:
                featmap = tf.nn.relu(featmap, name=scope.name)

            return featmap

    def _pooling(self, featmap, ksize, stride=2):
        featmap = tf.nn.max_pool(featmap, ksize=[1, ksize, ksize, 1], strides=[1, stride, stride, 1], padding="SAME", data_format=self.data_format)
        return featmap

    def _concat(self, past_layer, new_layer):
        return tf.concat((past_layer, new_layer), axis=1)

    def inference(self, images):
        # conv1
        featmap = self._conv(images, ksize=3, channel_out=64, stride=2, name="conv1_1")
        featmap = self._conv(featmap, ksize=3, channel_out=64, relu=False, name="conv1_2")
        featmap = self._conv(featmap, ksize=3, channel_out=128, relu=False, name="conv1_3")
        featmap = self._pooling(featmap, ksize=3, stride=2)
        pool_1 = featmap
        print("pool1 shape = {}".format(pool_1.shape))
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv2_1_proj")
        conv2_1 = featmap
        print("conv2_1 shape = {}".format(conv2_1.shape))

        # conv2_1
        featmap = pool_1
        featmap = self._conv(featmap, ksize=1, channel_out=64, relu=False, name="conv2_1_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]]))
        print("pad shape = {}".format(featmap.shape))
        featmap = self._conv(featmap, ksize=3, channel_out=64, relu=False, name="conv2_1")
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv2_1_increase")
        conv2_1_increase = featmap
        print("conv2_1_increase shape = {}".format(conv2_1_increase.shape))

        # conv2_2
        featmap = tf.add(conv2_1, conv2_1_increase)
        featmap = tf.nn.relu(featmap)
        conv2_1_relu = featmap
        print("conv2_1_relu shape = {}".format(conv2_1_relu.shape))
        featmap = self._conv(featmap, ksize=1, channel_out=64, relu=False, name="conv2_2_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]]))
        featmap = self._conv(featmap, ksize=3, channel_out=64, relu=False, name="conv2_2")
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv2_2_increase")
        conv2_2_increase = featmap

        # conv2_3
        featmap = tf.add(conv2_1_relu, conv2_2_increase)
        featmap = tf.nn.relu(featmap)
        conv2_2_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=64, relu=False, name="conv2_3_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]]))
        featmap = self._conv(featmap, ksize=3, channel_out=64, relu=False, name="conv2_3")
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv2_3_increase")
        conv2_3_increase = featmap

        # special1
        featmap = tf.add(conv2_2_relu, conv2_3_increase)
        featmap = tf.nn.relu(featmap)
        conv2_3_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, stride=2, name="conv3_1_proj")
        conv3_1_proj = featmap

        # conv3_1
        featmap = conv2_3_relu
        featmap = self._conv(featmap, ksize=1, channel_out=128, relu=False, stride=2, name="conv3_1_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]]))
        featmap = self._conv(featmap, ksize=3, channel_out=128, relu=False, name="conv3_1")
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, name="conv3_1_increase")
        conv3_1_increase = featmap

        # conv3_2
        featmap = tf.add(conv3_1_proj, conv3_1_increase)
        featmap = tf.nn.relu(featmap)
        conv3_1_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=128, relu=False, name="conv3_2_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]]))
        featmap = self._conv(featmap, ksize=3, channel_out=128, relu=False, name="conv3_2")
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, name="conv3_2_increase")
        conv3_2_increase = featmap

        # conv3_3
        featmap = tf.add(conv3_1_relu, conv3_2_increase)
        featmap = tf.nn.relu(featmap)
        conv3_2_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=128, relu=False, name="conv3_3_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]]))
        featmap = self._conv(featmap, ksize=3, channel_out=128, relu=False, name="conv3_3")
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, name="conv3_3_increase")
        conv3_3_increase = featmap

        # conv3_4
        featmap = tf.add(conv3_2_relu, conv3_3_increase)
        featmap = tf.nn.relu(featmap)
        conv3_3_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=128, relu=False, name="conv3_4_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [1, 1], [1, 1], [0, 0]]))
        featmap = self._conv(featmap, ksize=3, channel_out=128, relu=False, name="conv3_4")
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, name="conv3_4_increase")
        conv3_4_increase = featmap

        # special2
        featmap = tf.add(conv3_3_relu, conv3_4_increase)
        featmap = tf.nn.relu(featmap)
        conv3_4_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=1024, relu=False, name="conv4_1_proj")
        conv4_1_proj = featmap

        # conv4_1
        featmap = conv3_4_relu
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv4_1_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [2, 2], [2, 2], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=256, rate=2, relu=False, name="conv4_1")
        featmap = self._conv(featmap, ksize=1, channel_out=1024, relu=False, name="conv4_1_increase")
        conv4_1_increase = featmap

        # conv4_2
        featmap = tf.add(conv4_1_proj, conv4_1_increase)
        featmap = tf.nn.relu(featmap)
        conv4_1_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv4_2_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [2, 2], [2, 2], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=256, rate=2, relu=False, name="conv4_2")
        featmap = self._conv(featmap, ksize=1, channel_out=1024, relu=False, name="conv4_2_increase")
        conv4_2_increase = featmap

        # conv4_3
        featmap = tf.add(conv4_1_relu, conv4_2_increase)
        featmap = tf.nn.relu(featmap)
        conv4_2_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv4_3_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [2, 2], [2, 2], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=256, rate=2, relu=False, name="conv4_3")
        featmap = self._conv(featmap, ksize=1, channel_out=1024, relu=False, name="conv4_3_increase")
        conv4_3_increase = featmap

        # conv4_3
        featmap = tf.add(conv4_2_relu, conv4_3_increase)
        featmap = tf.nn.relu(featmap)
        conv4_3_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv4_4_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [2, 2], [2, 2], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=256, rate=2, relu=False, name="conv4_4")
        featmap = self._conv(featmap, ksize=1, channel_out=1024, relu=False, name="conv4_4_increase")
        conv4_4_increase = featmap

        # conv4_4
        featmap = tf.add(conv4_3_relu, conv4_4_increase)
        featmap = tf.nn.relu(featmap)
        conv4_4_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv4_5_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [2, 2], [2, 2], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=256, rate=2, relu=False, name="conv4_5")
        featmap = self._conv(featmap, ksize=1, channel_out=1024, relu=False, name="conv4_5_increase")
        conv4_5_increase = featmap

        # conv4_5
        featmap = tf.add(conv4_4_relu, conv4_5_increase)
        featmap = tf.nn.relu(featmap)
        conv4_5_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=256, relu=False, name="conv4_6_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [2, 2], [2, 2], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=256, rate=2, relu=False, name="conv4_6")
        featmap = self._conv(featmap, ksize=1, channel_out=1024, relu=False, name="conv4_6_increase")
        conv4_6_increase = featmap

        # special3
        featmap = tf.add(conv4_5_relu, conv4_6_increase)
        featmap = tf.nn.relu(featmap)
        conv4_6_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=2048, relu=False, name="conv5_1_proj")
        conv5_1_proj = featmap

        # conv5_1
        featmap = conv4_6_relu
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, name="conv5_1_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [4, 4], [4, 4], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=512, rate=4, relu=False, name="conv5_1")
        featmap = self._conv(featmap, ksize=1, channel_out=2048, relu=False, name="conv5_1_increase")
        conv5_1_increase = featmap

        # conv5_2
        featmap = tf.add(conv5_1_proj, conv5_1_increase)
        featmap = tf.nn.relu(featmap)
        conv5_1_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, name="conv5_2_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [4, 4], [4, 4], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=512, rate=4, relu=False, name="conv5_2")
        featmap = self._conv(featmap, ksize=1, channel_out=2048, relu=False, name="conv5_2_increase")
        conv5_2_increase = featmap

        # conv5_3
        featmap = tf.add(conv5_1_relu, conv5_2_increase)
        featmap = tf.nn.relu(featmap)
        conv5_2_relu = featmap
        featmap = self._conv(featmap, ksize=1, channel_out=512, relu=False, name="conv5_3_reduce")
        featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [4, 4], [4, 4], [0, 0]]))
        featmap = self._atrous_conv(featmap, ksize=3, channel_out=512, rate=4, relu=False, name="conv5_3")
        featmap = self._conv(featmap, ksize=1, channel_out=2048, relu=False, name="conv5_3_increase")
        conv5_3_increase = featmap

        # special4
        featmap = tf.add(conv5_2_relu, conv5_3_increase)
        featmap = tf.nn.relu(featmap)
        conv5_3 = featmap
        print(conv5_3.shape)

        # conv5_3_pool1



        return featmap

    def loss(self, logits, labels):
        cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=logits)
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
        tf.add_to_collection('losses', cross_entropy_mean)

        return tf.add_n(tf.get_collection('losses'), name='total_loss')

network = Network(is_training=True)
network.inference(tf.zeros([64, 101, 101, 1]))

# featmap = tf.ones(shape=(64, 3, 3, 3))
# featmap = tf.pad(featmap, paddings=tf.constant([[0, 0], [0, 0], [1, 1], [1, 1]]))
# sess = tf.Session()
# print(sess.run(featmap)[0][0])