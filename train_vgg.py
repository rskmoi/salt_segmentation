import datetime
import time

import tensorflow as tf

from dataset.vgg_dataset import Dataset
from network.vgg16 import Network


def train(loss, gloval_step):
    lr = tf.train.exponential_decay(0.001,
                                    global_step=gloval_step,
                                    decay_steps=10000,
                                    decay_rate=0.1,
                                    staircase=True)

    tf.summary.scalar('learning_rate', lr)

    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    minimize_op = optimizer.minimize(loss, global_step=gloval_step)
    update_op = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

    # logging
    loss_averages_op = _add_loss_summaries(loss)
    for var in tf.trainable_variables():
        tf.summary.histogram(var.op.name, var)

    train_op = tf.group((minimize_op, update_op, loss_averages_op))

    return train_op

def _add_loss_summaries(total_loss):
    loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
    losses = tf.get_collection('losses')
    loss_averages_op = loss_averages.apply(losses + [total_loss])

    for l in losses + [total_loss]:
        tf.summary.scalar(l.op.name + ' (raw)', l)
        tf.summary.scalar(l.op.name, loss_averages.average(l))

    return loss_averages_op

def start_train():
    dataset = Dataset(img_size=90, is_train=True, batch_size=64)
    network = Network(is_training=True)
    global_step = tf.Variable(0, name='global_step', trainable=False)

    images, labels, names = dataset.get_batch()

    logits = network.inference(images)
    loss = network.loss(logits, labels)

    train_op = train(loss, global_step)

    summary_op = tf.summary.merge_all()
    summary_writer = tf.summary.FileWriter("./output/log")

    _step = 0
    _start_time = time.time()

    saver = tf.train.Saver()
    with tf.train.SingularMonitoredSession(hooks=[tf.train.StopAtStepHook(last_step=50000)]) as sess:
        dataset.initialize(sess.raw_session())

        while not sess.should_stop():
            _loss, _ = sess.run([loss, train_op])
            if _step % 10 == 0:
                current_time = time.time()
                duration = current_time - _start_time
                _start_time = current_time

                examples_per_sec = 10 * 64 / duration
                sec_per_batch = float(duration / 10)

                format_str = ('%s: step %d, loss = %.2f (%.1f examples/sec; %.3f '
                              'sec/batch)')
                print(format_str % (datetime.datetime.now(), _step, _loss,
                                    examples_per_sec, sec_per_batch))

            if _step % 100 == 0:
                summary_writer.add_summary(sess.run(summary_op), _step)

            if _step % 1000 == 0:
                saver.save(sess.raw_session(), "./output/parameter/param", global_step=_step)

            _step += 1

if __name__ == "__main__":
    start_train()