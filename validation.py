import os
import time

import numpy as np
import pandas as pd
from inference_util.inference_result import InferenceResult, SaltResult
import pickle
from PIL import Image

import pydensecrf.densecrf as dcrf
from skimage.io import imread, imsave
from pydensecrf.utils import unary_from_labels, create_pairwise_bilateral
from skimage.color import gray2rgb
from skimage.color import rgb2gray
import matplotlib.pyplot as plt
import math

def main():
    use_crf = True
    pickle_path = "../results/result_out_5_0830_052754_30000_val.pickle"
    with open(pickle_path, "rb") as f:
        inf_res = pickle.load(f)

    for idx in range(50):
        threhsold = 0.3 + 0.01 * idx
        score_list = []
        for num_images, cur_res in enumerate(inf_res):
            if num_images % 100 == 0:
                pass
                #print("done {} images".format(num_images))

            cur_name = cur_res.name
            cur_map = cur_res.map
            cur_pred = np.where(cur_map > threhsold, 255, 0)

            cur_label = get_original_label(cur_name)
            if use_crf:
                cur_image = get_original_image(cur_name)
                cur_pred = crf(cur_image, cur_pred) * 255

            cur_iou = calculate_iou(cur_pred, cur_label)
            cur_score = calculate_score(cur_iou)
            score_list.append(cur_score)

        print("threshold {} : mean iou = {}".format(threhsold, np.mean(score_list)))



def get_original_image(name):
    image_path = "../data/train_5fold/images/{}.png".format(name)
    return np.asarray(Image.open(image_path))

def get_original_label(name):
    image_path = "../data/train_5fold/masks_5/{}.png".format(name)
    return np.asarray(Image.open(image_path).convert("L"))

def calculate_iou(pred, label):
    pred = pred / 255.0
    label = label / 255.0

    base_arr = pred + label
    intersect = np.where(base_arr == 2.0, 1.0, 0.0)
    union = base_arr - intersect
    intersect_sum = np.sum(intersect)
    union_sum = np.sum(union)

    if union_sum == 0.0:
        return 1.0

    iou = intersect_sum / union_sum

    return iou

def calculate_score(iou):
    if iou == 1.0:
        return 1.0
    score = None
    for i in range(11):
        cur_th = 0.5 + i * 0.05
        if iou < cur_th:
            score = i / 10.0
            break

    return score

def crf(original_image, mask_img):
    # Converting annotated image to RGB if it is Gray scale
    mask_img = gray2rgb(mask_img.reshape((101, 101)))

    #Converting the annotations RGB color to single 32 bit integer
    annotated_label = mask_img[:, :, 0] + (mask_img[:, :, 1] << 8) + (mask_img[:, :, 2] << 16)

    # Convert the 32bit integer color to 0,1, 2, ... labels.
    colors, labels = np.unique(annotated_label, return_inverse=True)

    n_labels = 2

    # Setting up the CRF model
    d = dcrf.DenseCRF2D(original_image.shape[1], original_image.shape[0], n_labels)

    # get unary potentials (neg log probability)
    U = unary_from_labels(labels, n_labels, gt_prob=0.7, zero_unsure=False)
    d.setUnaryEnergy(U)

    # This adds the color-independent term, features are the locations only.
    d.addPairwiseGaussian(sxy=(3, 3), compat=3, kernel=dcrf.DIAG_KERNEL,
                          normalization=dcrf.NORMALIZE_SYMMETRIC)

    # Run Inference for 10 steps
    Q = d.inference(10)

    # Find out the most probable class for each pixel.
    MAP = np.argmax(Q, axis=0)

    return MAP.reshape((original_image.shape[0], original_image.shape[1]))

if __name__ == "__main__":
    main()