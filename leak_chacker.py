import glob
import numpy as np
import hashlib
from PIL import Image
import os
import shutil

def calculate_max_spot_2d(spot_1d):
    col = spot_1d // 91
    row = spot_1d - (91 * col)

    return col+5, row+5



image_path_list_train = glob.glob("/home/rei/salt/data/train_5fold/images/*.png")
image_path_list_test = glob.glob("/home/rei/salt/data/test/*.png")

hash_list = []
hash_path_list = []
for image_path in image_path_list_train:
    cur_image = np.asarray(Image.open(image_path).convert("L"))
    cur_image_for_hash = cur_image[5:96, 5:96]

    col, row = calculate_max_spot_2d(cur_image_for_hash.argmax())
    cur_image = np.stack((cur_image, cur_image, cur_image)).transpose((1, 2, 0))
    cur_image = cur_image[col-5:col+5, row-5:row+5, :]
    cur_hash = hashlib.sha256(cur_image.tostring()).hexdigest()

    if cur_hash in hash_list:
        if np.sum(cur_image) != 0.0:
            pass
            # print(image_path)
            # print(cur_hash)

    hash_list.append(cur_hash)
    hash_path_list.append(image_path)
    # Image.fromarray(cur_image).save("./leak_test/{}".format(os.path.basename(image_path)))

leak_count = 0
for test_image_path in image_path_list_test:
    cur_image = np.asarray(Image.open(test_image_path).convert("L"))
    cur_image_for_hash = cur_image[5:96, 5:96]

    col, row = calculate_max_spot_2d(cur_image_for_hash.argmax())
    cur_image = np.stack((cur_image, cur_image, cur_image)).transpose((1, 2, 0))
    cur_image = cur_image[col-5:col+5, row-5:row+5, :]
    cur_hash = hashlib.sha256(cur_image.tostring()).hexdigest()

    if cur_hash in hash_list:
        if np.sum(cur_image) != 0.0:
            print(test_image_path)
            print(hash_path_list[hash_list.index(cur_hash)])
            print(cur_hash)
            leak_count += 1
            shutil.copy(src=test_image_path, dst="./leak_test/{}_test.png".format(leak_count))
            shutil.copy(src=hash_path_list[hash_list.index(cur_hash)], dst="./leak_test/{}_train.png".format(leak_count))

print(leak_count)