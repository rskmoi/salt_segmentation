import pickle

class SaltResult:
    def __init__(self, name, map):
        self.name = name
        self.map = map

class InferenceResult:
    def __init__(self, model_path):
        self.model_path = model_path
        self.results = []

    def append(self, salt_result):
        if not isinstance(salt_result, SaltResult):
            raise ValueError()

        self.results.append(salt_result)

    def save(self, path):
        with open(path, mode="wb") as f:
            pickle.dump(self, f)

    def num_images(self):
        return len(self.results)

    def __iter__(self):
        return iter(self.results)