import os
import numpy as np
import tensorflow as tf
from dataset.king_dataset import Dataset
from inference_util.inference_result import InferenceResult, SaltResult
from network.u_net import Network


def main():
    dataset = Dataset(img_size=101, mode="validation", batch_size=64, use_depths=False, use_set=[2])
    # dataset = Dataset(img_size=101, mode="test", batch_size=64, use_depths=False)
    network = Network(is_training=False)

    images, names = dataset.get_batch()
    images = tf.reshape(images, (-1, 1, 101, 101))

    flip = False
    if flip:
        logits_01 = network.inference(images)
        flipped_images = tf.reverse(images, [3])
        tf.get_variable_scope().reuse_variables()
        logits_02 = tf.reverse(network.inference(flipped_images), [3])

        logits = 0.5 * (logits_01 + logits_02)
    else:
        logits = network.inference(images)

    predict = tf.sigmoid(logits)

    _step = 0
    saver = tf.train.Saver()
    with tf.train.SingularMonitoredSession() as sess:
        dataset.initialize(sess.raw_session())
        ckpt = tf.train.get_checkpoint_state("../results/result_out_2_0831_042456/parameter")
        if ckpt and ckpt.model_checkpoint_path:
            model_path = ckpt.model_checkpoint_path
            model_path = model_path.replace("40000", "30000")
            print(model_path)
            saver.restore(sess, model_path)
            global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
        else:
            print('No checkpoint file found')
            return

        inf_res = InferenceResult(model_path)
        while not sess.should_stop():
            _preds, _names = sess.run([predict, names])
            _step += 1

            print("infered {} images".format(_step * 64))
            for cur_pred, cur_name in zip(_preds, _names):
                cur_img_name = os.path.basename(cur_name.decode("utf-8")).replace(".png", "")
                cur_pred = np.transpose(cur_pred, (1, 2, 0))
                inf_res.append(SaltResult(cur_img_name, cur_pred))


    inf_res.save("../results/{}_30000_val.pickle".format(model_path.split("/")[-3]))

if __name__ == "__main__":
    main()
